# import standard libraries
import logging, json, time

# configuring logging
FORMAT='%(asctime)s %(levelname)s %(message)s'
logging.basicConfig(filename='./logs/flaskapp.log', level=logging.INFO, format=FORMAT)
logging.Formatter.converter = time.gmtime
logger = logging.getLogger(__name__)

# import application modules
from channels import altadefinizioneclick
from channels import cineblog01
from channels import casacinema
from channels import geniodellostreaming
from channels import italiafilm
from channels import serietvu
from channels import seriehd
from channels import guardaserie
from channels import cineblogrun
from channels import filmsenzalimiti

from modules import tmdb, urlresolver
from modules.connector import DbConnector
from modules import helper
import concurrent.futures
from modules.helper import ShowType, compare_strings

# import third party libraries
from flask import Flask, g
from flask_cors import CORS
from celery import Celery
app = Flask(__name__)
CORS(app) # enable cors

# configuring celery
app.config['CELERY_BROKER_URL'] = 'amqp://admin:mypass@rabbit:5672//'
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

def get_db():
	if not hasattr(g, 'maria_db'):
		g.sqlite_db = DbConnector()
	return g.sqlite_db

@app.teardown_appcontext
def close_db(error):
	if hasattr(g, 'maria_db'):
		g.sqlite_db.close()


from flask import request


@celery.task
def update_database_async(show_id, season, episode, target):
    """
    Background task, get user request and update value in the database
    """
#    with app.app_context():
    logger.info("[CELERY] Looking for %s %s x %s", show_id, str(season), str(episode))
    db = DbConnector()

    # Looking for show info, put the show into DB
    search_info = tmdb.getInfo('', show_id, target)
    title = helper.clean(search_info['name'])
    original_name = helper.clean(search_info['original_name'])
    poster_path = search_info['poster_path']
    plot = search_info['overview']
    cover = search_info['backdrop_path']
    db.putShowOnDB(show_id, title, plot, poster_path, cover, target)

    # parse online websites
    results = []
    results += search_tvshow(title, season, episode)
    if not compare_strings(title, original_name):
            results += search_tvshow(original_name, season, episode)

    # add episodes on db
    logger.info("[CELERY] %s results found for %s", str(len(results)), title)
    for r in results:
        ourl = r['originalUrl']
        purl = r['parsedUrl']
        cha = r['channel']
        ser = r['server']
        sub = r.get('sub')
        quality = r.get('quality')
        db.putEpisodeOnDB(show_id, episode, season, cha, ser, ourl, purl, sub, quality, '1')

    # commit
    db.commit()

    # close connection
    logger.info('[CELERY] Closing DB')
    db.close()

@app.route("/movie")
def movie():
    """
    Entry point for movies
    params:
    - showID (number): number tmdb show id
    - title (string): optional
    """
    show_id = helper.convert_to_str(request.args.get('showId'))
    title = helper.convert_to_str(request.args.get('title'))


    # title is an optional parameters
    # we need to retrieve the title back from
    # tmdb using tmdb
    info = tmdb.getInfo('', show_id, ShowType.MOVIE)

    results = []
    if title and not compare_strings(title, info['title']):
        # title is specified and differes from the original one
        title = helper.clean(title)
        results = search_movie(title)
    else:
        # search using the title from tmdb
        title = helper.clean(info['title'])
        original_title = helper.clean(info['original_title'])

        results = search_movie(title)
        if len(results) == 0 and not compare_strings(title, original_title):
            # if no results and the title is different from the original title
            results = search_movie(original_title)

    results_no_duplicates = helper.remove_duplicates(results)

    logger.info("[Movie] %s", title)
    res = {'videos': results_no_duplicates}
    json_res = json.dumps(res)
    return json_res, 200, {'Content-Type': 'application/json'}

@app.route("/testdb")
def testdb():
    """
    Debug methods to test if db works
    """
    db = get_db()
    res = db.getEpisode(19885, 1, 1)
    json_res = json.dumps(res)
    return json_res, 200, {'Content-Type': 'application/json'}

@app.route("/toplist")
def toplist():
    """
    Return toplists of the target
    - target (tv or movie)
    - showID (string): if specified, use reccomandation
    """
    target = helper.convert_to_str(request.args.get('target'))
    target = helper.string_to_showtype(target)
    show_id = helper.convert_to_str(request.args.get('showId'))
    logger.info("[TOPLIST] %s", target)
    res = []
    if show_id:
        res = tmdb.getSuggestedShows(target, show_id)
    else:
        res = tmdb.getPopularShows(target)
    json_res = json.dumps(res)
    return json_res, 200, {'Content-Type': 'application/json'}

@app.route("/search")
def search():
    """
    Search title on tmdb
    """
    title = helper.convert_to_str(request.args.get('title'))
    target = helper.convert_to_str(request.args.get('target'))
    target = helper.string_to_showtype(target)
    logger.info("[SEARCH] %s - %s", target, title)
    title = title.encode('ascii', 'ignore')
    search_info = tmdb.search(title, target)
    json_res = json.dumps(search_info)
    return json_res, 200, {'Content-Type': 'application/json'}

@app.route("/info")
def getInfo():
    """
    Get info from tmdb
    - title (string, optional)
    - showId (string)
    - target
    """
    title = helper.convert_to_str(request.args.get('title'))
    show_id = helper.convert_to_str(request.args.get('showId'))
    target = helper.convert_to_str(request.args.get('target'))
    target = helper.string_to_showtype(target)
    logger.info("[INFO] %s - %s - %s", target, title, show_id)
    if title:
        title = title.encode('ascii', 'ignore')
    search_info = tmdb.getInfo(title, show_id, target)
    res = {'infoLabels': search_info}
    json_res = json.dumps(res)
    return json_res, 200, {'Content-Type': 'application/json'}

@app.route("/seasoninfo")
def getSeasonInfo():
    """
    Returns info for the selected season
    """ 
    show_id = helper.convert_to_str(request.args.get('showId'))
    season = helper.convert_to_str(request.args.get('season'))
    logger.info("[SEASON INFO] %s", show_id)
    season_info = tmdb.getSeasonInfo(show_id, season)
    res = {'seasonInfo': season_info}
    json_res = json.dumps(res)
    return json_res, 200, {'Content-Type': 'application/json'}

@app.route("/resolveurl")
def resolve_from_url():
    """
    Run urlresolver.resolve(url)
    - url (string)
    """
    url = helper.convert_to_str(request.args.get('url'))
    results = []
    if urlresolver.isValid(url):
        parsed_url, server = urlresolver.resolve(url)
        if parsed_url:
        	results.append({'parsedUrl': parsed_url, 'originalUrl': url, 'server': server})
        else:
            results.append({'originalUrl': url, 'server': server})
    res = {'videos': results} 
    json_res = json.dumps(res)
    return json_res, 200, {'Content-Type': 'application/json'}

@app.route("/resolvepage", methods=['POST'])
def resolve_from_page():
    """
    Run urlresolver.resolve(url)
    - url (string)
    - page_content (string)
    """
    url = request.form.get('url')
    content = request.form.get('content')    
    results = []
    if urlresolver.isValid(url):
        print('is-valid')
        parsed_url, server = urlresolver.resolve_from_page(url, content)
        if parsed_url:
        	results.append({'parsedUrl': parsed_url, 'originalUrl': url, 'server': server})
        else:
            results.append({'originalUrl': url, 'server': server})
    res = {'videos': results} 
    json_res = json.dumps(res)
    return json_res, 200, {'Content-Type': 'application/json'}

def search_show_on_db(show_id, season, episode):
    db = get_db()
    db_results = db.getEpisode(show_id, season, episode)
    results = []
    if db_results:
        logger.info("possible results on db")
        # find results from db
        # update parsedUrl and delete expired items
        for r in db_results:
            r['originalUrl'] = r['original_url']
            if not urlresolver.isValid(r['originalUrl']):
                logger.info(r['originalUrl'] + ' is not valid anymore')
                db.deleteLink(r['originalUrl'])
                continue
            # if is still valid, add to results
            logger.info("adding valid result from db source" +  r['originalUrl'])
            results.append(r)
    return results

@app.route("/tv")
def tvshow():
    """
    Entry point for tv shows
    - showId
    - episode
    - season
    - use_cache (search on db first)
    """
    show_id = helper.convert_to_str(request.args.get('showId'))
    episode = helper.convert_to_str(request.args.get('episode'))
    season = helper.convert_to_str(request.args.get('season'))
    use_cache = helper.convert_to_str(request.args.get('cache'))
    logger.info("[TV] ID: %s  E: %s  S: %s", show_id, episode, season)

    results = []
    source = None

    if use_cache != 'false':
        logger.info("looking in the db")
        source = 'db'
        results = search_show_on_db(show_id, season, episode)

    if not results:
        logger.info("looking in the internet")
        # nothing useful found on db, do an online research
        # since the search works with the title
        # we need to get it from tmdb database first
        source = 'online'

        # put information about the Show
        search_info = tmdb.getInfo('', show_id, ShowType.TV)
        title = helper.clean(search_info['name'])
        original_name = helper.clean(search_info['original_name'])

        # run parser
        results = search_tvshow(title, season, episode)
        if len(results) == 0 and not compare_strings(title, original_name):
                results = search_tvshow(original_name, season, episode)

    logger.info("[TV] ID: %s - source: %s", show_id, source)

    results_no_duplicates = helper.remove_duplicates(results)

    res = {'videos': results_no_duplicates}
    json_res = json.dumps(res)
    update_database_async.delay(show_id, season, episode, ShowType.TV)
    return json_res, 200, {'Content-Type': 'application/json'}

def download_tvshow_from_source(source):
    name = source['name']
    service_class = source['service']
    title = source['title']
    season = source['season']
    episode = source['episode']

    results = []
    try:
        service = service_class()
        search_list = service.search(title, ShowType.TV)
        for page in search_list:
            # 'game of thrones' in page['title'].lower() -- sad way to match game of thrones in all those sites
            # that have "Games of Thrones - Il Trono di spade" as original title
            if compare_strings(title, page['title']) or 'game of thrones' in page['title'].lower():
                res = service.find_episode(page['url'], season, episode)
                if res:
                    for vid in res:
                        vid['channel'] = name
                    results += res
    except Exception as e:
        logger.exception("runserver - download_movie_from_source")

    return name, results

def search_tvshow(title, season, episode):
    season = int(season)
    episode = int(episode)

    # channels
    sources = [
        {'name': 'serietvu', 'service': serietvu.Serietvu, 'title': title, 'season': season, 'episode': episode},
        {'name': 'seriehd', 'service': seriehd.Seriehd, 'title': title, 'season': season, 'episode': episode},
        {'name': 'geniostreaming', 'service': geniodellostreaming.Geniostreaming,'title': title, 'season': season, 'episode': episode},
        {'name': 'cb01', 'service': cineblog01.Cineblog, 'title': title, 'season': season, 'episode': episode},
        {'name': 'guardaserie', 'service': guardaserie.Guardaserie, 'title': title, 'season': season, 'episode': episode},
    ]

    merge_results = []
    with concurrent.futures.ThreadPoolExecutor() as executor:
        for name, results in executor.map(download_tvshow_from_source, sources):
            logger.info("[TV] from %s got %d", name, len(results))
            merge_results += results
    
    return merge_results


def download_movie_from_source(source):
    name = source['name']
    service_class = source['service']
    title = source['title']
    results = []
    try:
        service = service_class()
        search_list = service.search(title, ShowType.MOVIE)
        for page in search_list:
            if compare_strings(title, page['title']):
                res = service.find_movie(page['url'])
                if res:
                    for vid in res:
                        vid['channel'] = name
                    results += res

    except Exception as e:
        logger.exception("runserver - download_movie_from_source")

    return name, results

def search_movie(title):
    # channels
    sources = [
        {'name': 'filmsenzalimiti', 'service': filmsenzalimiti.Filmsenzalimiti, 'title': title},
        {'name': 'cbrun', 'service': cineblogrun.CineblogRun, 'title': title},
        {'name': 'casacinema', 'service': casacinema.Casacinema, 'title': title},
        {'name': 'altadefinizione', 'service': altadefinizioneclick.AltaDefinizioneClick, 'title': title},
        {'name': 'cb01', 'service': cineblog01.Cineblog, 'title': title},
        {'name': 'geniostreaming', 'service': geniodellostreaming.Geniostreaming, 'title': title},
        {'name': 'italiafilm', 'service': italiafilm.Italiafilm, 'title': title },
    ]

    merge_results = []
    with concurrent.futures.ThreadPoolExecutor() as executor:
        for name, results in executor.map(download_movie_from_source, sources):
            logger.info("[MOVIE] from %s got %d", name, len(results))
            merge_results += results
    
    return merge_results

if __name__ == "__main__":   
    app.run(host='0.0.0.0', port=4567)

