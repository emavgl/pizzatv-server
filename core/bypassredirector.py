import re
import requests
from base64 import b64decode

HTTP_HEADER = {
    "User-Agent": 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36',
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Encoding": "gzip,deflate,sdch",
    "Connection": "keep-alive",
    "Accept-Language": "nl-NL,nl;q=0.8,en-US;q=0.6,en;q=0.4",
    "Cache-Control": "no-cache",
    "Pragma": "no-cache"
}

def bypass_adfly(uri):
    try:
        r = requests.get(uri, headers=HTTP_HEADER, timeout=10)
        html = r.text
        ysmm = re.findall(r"var ysmm =.*\;?", html)
        if len(ysmm) > 0:
            ysmm = re.sub(r'var ysmm \= \'|\'\;', '', ysmm[0])
            left = ''
            right = ''
            for c in [ysmm[i:i+2] for i in range(0, len(ysmm), 2)]:
                left += c[0]
                right = c[1] + right
            decoded_uri = b64decode(left.encode() + right.encode())[2:].decode()
            if re.search(r'go\.php\?u\=', decoded_uri):
                decoded_uri = b64decode(re.sub(r'(.*?)u=', '', decoded_uri)).decode()
            return decoded_uri, r.status_code
        else:
            return uri, 'No ysmm variable found'
    except Exception as e:
        return uri, str(e)
