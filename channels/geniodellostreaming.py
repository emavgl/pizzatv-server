import re
import requests
from bs4 import BeautifulSoup
import base64
from core import scrapertools
from modules import urlresolver
import logging
from modules.helper import ShowType
from core import httptools


logger = logging.getLogger(__name__)

class Geniostreaming:
    def __init__(self):
        self.host = "http://ilgeniodellostreaming.org"
        self.headers = {
            'User-Agent':  'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
            'Referer': self.host
        }
        self.session = scrapertools.open_session(self.host, cloudflare=True)


    def search(self, text, target):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def find_movie(self, show_url):
        html_doc, _ = scrapertools.anti_cloudflare_force(show_url, self.session)
        soup = BeautifulSoup(html_doc, 'html.parser')
        tbody = soup.find("tbody")
        rows = tbody.findAll('tr')

        results = []
        for row in rows:
            columns = row.findAll('td')
            isSub = 'sub-' in columns[-2].text.lower()
            resolutionText = columns[-3].text.lower()
            resolution = 'HD' if ('1080' in resolutionText or '720' in resolutionText or 'hd' in resolutionText) else 'SD'
            first_step_link = row.find("a", {'class': "link_a"})
            first_step_link = first_step_link.get('href')

            # get second step link
            html_doc, _ = scrapertools.anti_cloudflare_force(first_step_link, self.session)
            soup = BeautifulSoup(html_doc, 'html.parser')
            redirected_link = soup.find('a')

            if redirected_link:
                redirected_link = redirected_link.get('href')
            
                # check if valid
                if urlresolver.isValid(redirected_link):
                    link = {'quality': resolution, 'sub': isSub, 'server': 'openload', 'originalUrl': redirected_link, 'parsedUrl': '', 'channel': 'geniodellostreaming'}
                    results.append(link)

        # close session
        scrapertools.close_session(self.host, self.session)

        # return items
        return results
        

    def list_movies(self, text):
        try:
            text = text.replace(' ', '+')
            search_url =  self.host + "/?s=" + text
            
            item_list = []
            
            html_doc, _ = scrapertools.anti_cloudflare_force(search_url, self.session)
            soup = BeautifulSoup(html_doc, 'html.parser')
            found_items = soup.find_all("div", {'class': "thumbnail"})
            for item in found_items:
                thumbnail = item.find('img').get('src')
                url = item.find('a').get('href')
                title = item.find('img').get('alt')
                category = item.find('span').text

                if category.lower() != 'film':
                    continue

                #if '[Sub-ITA]' not in title and show_name in title.lower():
                item_list.append({'title': title, 'thumbnail': thumbnail, 'url': url})

            return item_list
        except Exception as e:
            print(e)
            return []

    def list_tvshow(self, text):
        try:
            text = text.replace(' ', '+')
            search_url =  self.host + "/?s=" + text
            
            item_list = []
            
            html_doc, _ = scrapertools.anti_cloudflare_force(search_url, self.session)
            soup = BeautifulSoup(html_doc, 'html.parser')
            found_items = soup.find_all("div", {'class': "thumbnail"})
            for item in found_items:
                thumbnail = item.find('img').get('src')
                url = item.find('a').get('href')
                title = item.find('img').get('alt')
                category = item.find('span').text

                if category.lower() != 'tv':
                    continue

                #if '[Sub-ITA]' not in title and show_name in title.lower():
                item_list.append({'title': title, 'thumbnail': thumbnail, 'url': url})

            return item_list
        except Exception as e:
            print(e)
            return []

    def find_episode(self, show_url, season = 1, episode = 1):
        print("searching in ", show_url)
        try:
            html_doc, _ = scrapertools.anti_cloudflare_force(show_url, self.session)
            soup = BeautifulSoup(html_doc, 'html.parser')
            season_elements = soup.find_all("div", {'class': "se-c"})

            result = None
            for season_element in season_elements:
                for episode_element in season_element.find_all("li"):
                    numerando_box = (episode_element.find("div", {'class': "numerando"})).text
                    s, e = numerando_box.split(' - ')
                    s = int(s)
                    e = int(e)
                    if e == episode and s == season:
                        result = {'url': episode_element.find('a').get('href'), 'episode': e, 'season': s}
                        break

                if result is not None:
                    break

            results = []

            if result:
                video_page = result.get('url')
                html_doc, _ = scrapertools.anti_cloudflare_force(video_page, self.session)
                soup = BeautifulSoup(html_doc, 'html.parser')
                requested_video_url = (soup.find("iframe", {'class': "metaframe"})).get('src')
                if urlresolver.isValid(requested_video_url):
                    tmp = {}
                    tmp['channel'] = 'geniostreaming'
                    tmp['server'] = 'openload'
                    tmp['sub'] = 'sub-ita' in show_url
                    tmp['quality'] = '-'
                    tmp['originalUrl'] = requested_video_url
                    tmp['episode'] = result.get('episode')
                    tmp['season'] = result.get('season')
                    tmp['parsedUrl'] = ''
                    results.append(tmp)

                # close session
                scrapertools.close_session(self.host, self.session)

                # return items
                return results

            elif 'sub-ita' not in show_url:
                # no-results found
                # try sub-ita
                show_url = show_url[:-1]
                show_url += '-sub-ita/'
                return self.find_episode(show_url, season, episode)

        except Exception as e:
            logger.exception("geniostreaming")
            return []