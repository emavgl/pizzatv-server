from core import scrapertools
import requests
from bs4 import BeautifulSoup
from modules.helper import ShowType
from modules import urlresolver
from modules import helper

import logging
logger = logging.getLogger(__name__)


class Italiafilm:
    def __init__(self):
        self.host = "http://www.italia-film.online/"
        self.headers = {
            'User-Agent':  'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
            'Referer': self.host
        }
        self.session = scrapertools.open_session(self.host)

    def search(self, text, target):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def find_episode(self, show_url, session, season = 1, episode = 1):
        raise NotImplementedError()

    def list_tvshow(self, text):
        raise NotImplementedError()

    def list_movies(self, text):
        itemlist = []

        search_url = "%s/?s=%s" % (self.host, text.replace(" ", "+"))
        data, _ = scrapertools.downloadpage(search_url, session=self.session, headers=self.headers)

        soup = BeautifulSoup(data, 'html.parser')
        for article in soup.findAll('article'):
            title_box = article.find('h3', {'class': 'entry-title'})
            clickable_title = title_box.find('a', href=True)
            title = clickable_title.text
            if not helper.compare_strings(title, text):
                continue
            url = clickable_title['href']
            itemlist.append({'title': title, 'url': url})

        return itemlist

    def find_movie(self, show_url):
        itemlist = []
        inserted_link = []

        data, _ = scrapertools.downloadpage(show_url, session=self.session, headers=self.headers)
        soup = BeautifulSoup(data, 'html.parser')
        content = soup.find('div', {'class': 'entry-content'})

        openload_links = content.findAll('a', href=lambda href: href and "openload" in href)
        openload_links += content.findAll('a', href=True, text='Openload')
        openload_links = list(set(openload_links))
        for link in openload_links:
            if link['href'] not in inserted_link: 
                inserted_link.append(link['href'])
                info = {'channel': 'italiafilm', 'server': 'openload', 'url': link['href'], 'quality': '-'}
                itemlist.append(info)

        rapid_links = content.findAll('a', href=lambda href: href and "rapidvideo" in href)
        rapid_links += content.findAll('a', href=True, text='RapidVideo')
        rapid_links = list(set(rapid_links))
        for link in rapid_links:
            if link['href'] not in inserted_link: 
                inserted_link.append(link['href'])
                info = {'channel': 'italiafilm', 'server': 'rapidvideo', 'url': link['href'], 'quality': '-'}
                itemlist.append(info)

        stream_links = content.findAll('a', href=lambda href: href and "streamango" in href)
        stream_links += content.findAll('a', href=True, text='StreamanG')
        stream_links = list(set(stream_links))
        for link in stream_links:
            if link['href'] not in inserted_link: 
                inserted_link.append(link['href'])
                info = {'channel': 'italiafilm', 'server': 'streamango', 'url': link['href'], 'quality': '-'}
                itemlist.append(info)

        content = soup.find('div', {'class': 'playerbox'})

        # Check for rapidvideo inside the box
        if content:
            box_url = content.find('iframe')['src']
            if 'rapidvideo' in box_url:
                info = {'channel': 'italiafilm', 'server': 'rapidvideo', 'url': box_url, 'quality': '-'}
                itemlist.append(info)

        parsed = []
        for item in itemlist:
            try:
                video_url = urlresolver.followRedirectUrl(item['url'])
                item['originalUrl'] = video_url
                extracted_url = item['originalUrl']
                if extracted_url and urlresolver.isValid(extracted_url):
                    item['parsedUrl'], _ = urlresolver.resolve(extracted_url)
                    parsed.append(item)
            except Exception as e:
                    logger.exception("italiafilm")
                    continue

        # close session
        scrapertools.close_session(self.host, self.session)
        
        return parsed

