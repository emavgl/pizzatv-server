import re
from core import fakelogger as logger
import requests

def isValid(url):
    try:
        r = requests.get(url, timeout=6)
        html_doc = r.text
        if r.status_code != 200 or 're Sorry' in html_doc:
            return False
        return True
    except:
        return False

def resolve(url, data):
    return ""