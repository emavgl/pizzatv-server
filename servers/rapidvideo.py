from core import jsunpack
import requests
from core import scrapertools

def isValid(url):

    if '.com' not in url:
        return False

    try:
        r = requests.get(url, timeout=6)
        html_doc = r.text
        if r.status_code != 200 or 'Not Found' in html_doc or '"sources": null' in html_doc:
            return False
        return True
    except Exception as e:
        return False

def resolve(page_url, data):
    urls = scrapertools.find_multiple_matches(data, '"file":"([^"]+.mp4)"')
    best_resolutions = urls[-1]
    return best_resolutions.replace('\\', '')