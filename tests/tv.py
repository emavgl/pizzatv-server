import unittest

import sys
sys.path.append("..")

from channels import serietvu
from channels import seriehd
from channels import geniodellostreaming
from channels import cineblog01
from channels import guardaserie
from modules.helper import ShowType

sources = [
    {'name': 'serietvu', 'service': serietvu.Serietvu},
    {'name': 'seriehd', 'service': seriehd.Seriehd},
    {'name': 'geniostreaming', 'service': geniodellostreaming.Geniostreaming},
    {'name': 'guardaserie', 'service': guardaserie.Guardaserie},
    {'name': 'cb01', 'service': cineblog01.Cineblog}
]

def download_from_source(source, custom_title=None):
    title = "mr robot"
    if custom_title:
        title = custom_title

    name = source['name']
    service_class = source['service']
    service = service_class()
    search_list = service.search(title, ShowType.TV)
    results = []
    if search_list:
        print('found')
        results = service.find_episode(search_list[0].get('url'), 1, 1)
    else:
        print("not found")
    return name, results

def sequential():
    merge_results = []
    for name, results in map(download_from_source, sources):
        merge_results += results
    print('merge_results %d' % (len(merge_results),))


class TestTVSources(unittest.TestCase):

    def test_cineblog(self):
        source = {'name': 'cb01', 'service': cineblog01.Cineblog}
        name, results = download_from_source(source)
        print(name, "> Extracted", len(results))
        self.assertTrue(len(results) > 0)

    def test_serietvu(self):
        source = {'name': 'serietvu', 'service': serietvu.Serietvu}
        name, results = download_from_source(source)
        print(name, "> Extracted", len(results))
        self.assertTrue(len(results) > 0)

    def test_seriehd(self):
        source = {'name': 'seriehd', 'service': seriehd.Seriehd}
        name, results = download_from_source(source)
        print(name, "> Extracted", len(results))
        self.assertTrue(len(results) > 0)

    def test_geniostreaming(self):
        source = {'name': 'geniostreaming', 'service': geniodellostreaming.Geniostreaming}
        name, results = download_from_source(source)
        print(name, "> Extracted", len(results))
        self.assertTrue(len(results) > 0)

    def test_guardaserie(self):
        source = {'name': 'guardaserie', 'service': guardaserie.Guardaserie}
        name, results = download_from_source(source)
        print(name, "> Extracted", len(results))
        self.assertTrue(len(results) > 0)

if __name__ == '__main__':
    unittest.main()