from urllib.parse import urlparse
import requests
import re
from bs4 import BeautifulSoup
from servers import nowvideo, rapidvideo, speedvideo, openload
from servers import wstream, vidgot, nowvideo, cloudifer, megadrive
from servers import vidtome, streamango, watchers
import logging
logger = logging.getLogger(__name__)

sources = {'speedvideo': speedvideo, 'rapidvideo': rapidvideo, 'openload': openload,
           'oload': openload, 'wstream': wstream, 'nowvideo': nowvideo, 'cloudifer': cloudifer,
           'megadrive': megadrive, 'vidto': vidtome, 'streamango': streamango}

# these can be parsed directly from the server
# url is not soggetted to IP restriction
from_url_extraction = []

# Due to IP restriction
# it is possible to extract the url
# from these services only from the webpage
# download via client
from_page_content_extraction = ['rapidvideo', 'vidto', 'speedvideo', 
                                'wstream', 'nowvideo', 'cloudifer', 
                                'megadrive', 'streamango']

# better handle openload via client only
from_client_only = ['oload', 'openload']


def isValid(url):

    # remove all those urls that ends with an archive extension (false video files)
    if '.rar' in url or '.zip' in url:
        return False

    try:
        website_name = getWebSiteName(url)
        if website_name in (from_url_extraction + from_client_only + from_page_content_extraction):
            source_module = sources[website_name]
            return source_module.isValid(url)
        return False
    except Exception as e:
        logger.exception("url_resolver_is_valid")
        logger.info(url + ' is not a valid website')
        return False

def getWebSiteName(url):
    return ((urlparse(url).hostname).replace("www.", "").split("."))[0]

def resolve(url):
    try:
        website_name = getWebSiteName(url)
        if website_name in from_url_extraction:
            source_module = sources[website_name]
            
            page_content = requests.get(url, timeout=5)

            if not page_content or page_content.status_code != 200:
                return "", website_name
            
            extracted_link = source_module.resolve(url, page_content.text)
            
            if extracted_link and not isinstance(extracted_link, str):
                extracted_link = extracted_link.decode('utf-8')
            
            return extracted_link, website_name
        return "", website_name
    except Exception as e:
        logger.exception("url_resolver_resolve " + url)
        return "", website_name

def resolve_from_page(url, page_content):
    try:
        website_name = getWebSiteName(url)
        print(website_name)
        if website_name in from_page_content_extraction:
            print('is handled')
            source_module = sources[website_name]
            extracted_link = source_module.resolve(url, page_content)
            if extracted_link and not isinstance(extracted_link, str):
                extracted_link = extracted_link.decode('utf-8')
            return extracted_link, website_name
        return "", website_name
    except Exception as e:
        logger.exception("url_resolver_resolve_from_page " + url)
        return "", website_name

def followRedirectUrl(url, session=None):
    if not session:
        res = requests.get(url)
    else:
        res = session.get(url)

    return res.url

def updateParsedUrl(o_url, p_url):
    """
    Params:
        o_url: originalUrl
        p_url: parsedUrl
    Return:
        valid parsedUrl
    """
    try:
        valid = isValid(o_url)
        if not valid:
            return None
        return (resolve(o_url))[0]
    except Exception as e:
        logger.exception("url_resolve_update_parsed_url")
        return ""

