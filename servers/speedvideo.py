import re
import base64
import requests
import logging

logger = logging.getLogger(__name__)

def isValid(url):
    try:
        r = requests.get(url, timeout=10)
        html_doc = r.text
        if 'is expired' in html_doc.lower() or 'file was deleted' in html_doc.lower():
            return False
        return True
    except Exception as e:
        logger.exception("speed_video_isvalid")
        return False

def resolve(page_url, data):
    try:
        if 'embed' not in page_url:
            if '.html' in page_url:
                page_url = 'http://speedvideo.net/embed-'  + page_url.split('/')[-2] + '-607x360.html'
            else:
                page_url = 'http://speedvideo.net/embed-'  + page_url.split('/')[-1] + '-607x360.html'
        
        if not data:
            data = requests.get(page_url, timeout=5).text

        matches = re.compile('linkfile ="([^"]+)"').findall(data)
         
        if matches:
            return matches[0]
        else:
            return ""
    except Exception as e:
        logger.exception("speed_video_resolve " + page_url)
        return None
