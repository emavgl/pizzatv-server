import re
from core import fakelogger as logger
from core import scrapertools
from servers import openload
from modules import urlresolver
from modules.helper import ShowType
import logging
from bs4 import BeautifulSoup
import base64

logger = logging.getLogger(__name__)

class Filmhd:
    def __init__(self):
        self.host = "http://filmhd.me"
        self.headers = {
            'User-Agent':  'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0'
        }
        self.session = scrapertools.open_session(self.host, cloudflare=True)

    def search(self, text, target):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def find_episode(self, show_url, season = 1, episode = 1):
        raise NotImplementedError()

    def list_tvshow(self, text):
        raise NotImplementedError()

    def list_movies(self, text):
        search_url = "%s/?s=%s" % (self.host, text)
        itemlist = []

        data, _ = scrapertools.anti_cloudflare_force(search_url, self.session)

        soup = BeautifulSoup(data, 'html.parser')
        items = soup.findAll('div', {'class': 'item'})
        for item in items:
            title = item.find('h2', {'class': 'movie-title'}).text
            url = (item.find('a'))['href']
            info = {'title': title, 'url': url}
            itemlist.append(info)
        
        return itemlist

    def find_movie(self, show_url):
        # get inner iframe with sources and resolutions
        data, _ = scrapertools.anti_cloudflare_force(show_url, self.session)
        soup = BeautifulSoup(data, 'html.parser')
        buttons = soup.findAll('li', {'id': 'streaming-hd'})
        
        results = []
        for button in buttons:
            link_node = button.find('a')

            # Resolution
            res = 'HD'
            if link_node.get('title') and '360' in link_node.get('title'):
                res = 'SD'
            
            original_url = link_node['href']
            if urlresolver.isValid(original_url):
                link = {'server': 'openload', 'originalUrl': original_url, 'quality': res, 'parsedUrl': '', 'channel': 'filmhd'}
                results.append(link)

        # close session
        scrapertools.close_session(self.host, self.session)

        return results
