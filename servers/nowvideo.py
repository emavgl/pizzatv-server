import re
import requests
from core import scrapertools
from core import jsunpack

def isValid(url):
    try:
        r = requests.get(url, timeout=6)
        html_doc = r.text
        if r.status_code != 200 or 'no longer exists' in html_doc or 'not yet ready' in html_doc:
            return False
        return True
    except Exception as e:
        return False

def resolve(url, data):
    videourls = scrapertools.find_multiple_matches(data, 'src\s*:\s*[\'"]([^\'"]+)[\'"]')
    if not videourls:
        videourls = scrapertools.find_multiple_matches(data, '<source src=[\'"]([^\'"]+)[\'"]')
    for video_url in videourls:
        if video_url.endswith(".mpd"):
            video_id = scrapertools.find_single_match(video_url, '/dash/(.*?)/')
            video_url = "http://www.nowvideo.li/download.php%3Ffile=mm" + "%s.mp4" % video_id
            video_url = re.sub(r'/dl(\d)*/', '/dl/', video_url)
            video_url = video_url.replace("%3F", "?")
            return video_url
        else:
            return video_url
    return ""