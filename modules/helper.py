import difflib
import logging
import re
logger = logging.getLogger(__name__)

def _remove_text_inside_brackets(text, brackets="()[]"):
    count = [0] * (len(brackets) // 2) # count open/close brackets
    saved_chars = []
    for character in text:
        for i, b in enumerate(brackets):
            if character == b: # found bracket
                kind, is_close = divmod(i, 2)
                count[kind] += (-1)**is_close # `+1`: open, `-1`: close
                if count[kind] < 0: # unbalanced bracket
                    count[kind] = 0  # keep it
                else:  # found bracket to remove
                    break
        else: # character is not a [balanced] bracket
            if not any(count): # outside brackets
                saved_chars.append(character)
    return ''.join(saved_chars)

def convert_to_str(data):
    if not data:
        return ''

    if not isinstance(data, str):
        data = data.decode('utf-8')
    return data

def clean(data):
    text = convert_to_str(data)

    # remove brackets
    no_brack = _remove_text_inside_brackets(text)

    # replace any not alphanumeric char with space
    alphanum = re.sub('[^0-9a-zA-Z ]+', ' ', no_brack)

    # encode in ascii
    new_str = alphanum

    # make it lower
    new_str = new_str.lower()

    # strip
    new_str = new_str.strip()

    # remove double spaces
    new_str = new_str.replace("  ", " ")

    return new_str

def compare_strings(str1, str2, margin=0.90):
    str1 = clean(str1)
    str2 = clean(str2)
    
    ratio = difflib.SequenceMatcher(None, str1, str2).ratio()

    # Compare also list of numbers in the way that
    # Cattivissimo Me 3 does not to match with Cattivissimo Me 2
    numbers1 = [int(s) for s in str1.split() if s.isdigit()]
    numbers2 = [int(s) for s in str2.split() if s.isdigit()]

    if ratio > margin and numbers1 == numbers2 :
        return True

    return False

def is_contained_in(str1, str2):
    str1 = clean(str1)
    str2 = clean(str2)
    return (str1 in str2)

def string_to_showtype(label):
    if label == 'tv':
        return ShowType.TV
    return ShowType.MOVIE

def remove_duplicates(list_vids):
    merged_list = []
    for video in list_vids:
        found = False
        for element in merged_list:
            if video['originalUrl'] == element['originalUrl']:
                found = True
                break;
        if not found: merged_list.append(video)
    return merged_list

class ShowType:
    """
    Enum Class
    """
    MOVIE = 0
    TV = 1