import re
import base64
from core import fakelogger as logger
from core import scrapertools
import requests
import logging

logger = logging.getLogger(__name__)

def isValid(url):
    try:
        r = requests.get(url, timeout=10)
        html_doc = r.text
        if 'Expired' in html_doc or 'removed by the owner' in html_doc:
            return False
        return True
    except Exception as e:
        logger.exception("streamango_resolve")
        return False

def resolve(page_url, data):
    try:
        url = scrapertools.find_single_match(data, 'src:"([^"]+)",')
        if 'http' not in url:
            url = 'http:' + url
        return url
    except Exception as e:
        logger.exception("streamango_resolve" + page_url)
        return None