import re
from urllib.parse import urljoin

#from core import httptools
from core import fakelogger as logger
from core import scrapertools
from servers import openload
from modules import urlresolver
from modules import helper
from bs4 import BeautifulSoup
from core import jsunpack
import cfscrape
from modules.helper import ShowType
import logging
logger = logging.getLogger(__name__)

class Cineblog:
    def __init__(self):
        self.host = "http://www.cb01.uno"
        self.headers = {
            'User-Agent':  'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
            'Referer': self.host
        }
        self.session = scrapertools.open_session(self.host, cloudflare=True)

    def search(self, text, target):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def list_movies(self, text):
        itemlist = []

        search_url = "%s/?s=%s" % (self.host, text.replace(" ", "+"))

        data, _ = scrapertools.anti_cloudflare_force(search_url, self.session)

        # Filter results
        patronvideos = '<div class="span4".*?<a.*?<p><img src="([^"]+)".*?'
        patronvideos += '<div class="span8">.*?<a href="([^"]+)"> <h1>([^"]+)</h1></a>.*?'
        patronvideos += '<strong>([^<]*)</strong>.*?<br />([^<+]+)'
        matches = re.compile(patronvideos, re.DOTALL).finditer(data)

        for match in matches:
            scrapedtitle = match.group(3)
            if not helper.compare_strings(scrapedtitle, text):
                continue
            scrapedurl = urljoin(search_url, match.group(2))
            itemlist.append({'title': scrapedtitle, 'url': scrapedurl})

        return itemlist

    def find_episode(self, show_url, season = 1, episode = 1):
        itemlist = []

        data, _ = scrapertools.anti_cloudflare_force(show_url, self.session)

        inserted_link = []
        soup = BeautifulSoup(data, 'html.parser')
        for table_body in soup.findAll('div', {'class': 'sp-body'}):
            for strong in table_body.findAll('p'):
                match = re.search(r'(\d{1,2}).(\d{1,2})', strong.text)
                if not match: continue
                s = int(match.group(1))
                e = int(match.group(2))
                
                if e == episode and s == season:
                    for link in strong.findAll('a', href=True, text='Openload'):
                        if link['href'] not in inserted_link: 
                            inserted_link.append(link['href'])
                            previous_header = link.find_previous('div', {'title': 'Expand'})
                            isSub = 'SUB' in previous_header.text
                            info = {'channel': 'cineblog01', 'server': 'openload', 'sub': isSub, 'url': link['href'], 'quality': 'SD'}
                            itemlist.append(info)

                    for link in strong.findAll('a', href=True, text='Wstream'):
                        if link['href'] not in inserted_link: 
                            inserted_link.append(link['href'])
                            previous_header = link.find_previous('div', {'title': 'Expand'})
                            isSub = 'SUB' in previous_header.text
                            info = {'channel': 'cineblog01', 'server': 'wstream', 'sub': isSub, 'url': link['href'], 'quality': 'SD'}
                            itemlist.append(info)

                    for link in strong.findAll('a', href=True, text='Streamango'):
                        if link['href'] not in inserted_link: 
                            inserted_link.append(link['href'])
                            previous_header = link.find_previous('div', {'title': 'Expand'})
                            isSub = 'SUB' in previous_header.text
                            info = {'channel': 'cineblog01', 'server': 'streamango', 'sub': isSub, 'url': link['href'], 'quality': 'SD'}
                            itemlist.append(info)

                    for link in strong.findAll('a', href=True, text='Nowvideo'):
                        if link['href'] not in inserted_link: 
                            inserted_link.append(link['href'])
                            previous_header = link.find_previous('div', {'title': 'Expand'})
                            isSub = 'SUB' in previous_header.text
                            info = {'channel': 'cineblog01', 'server': 'nowvideo', 'sub': isSub, 'url': link['href'], 'quality': 'SD'}
                            itemlist.append(info)               
        
        parsed = []
        for item in itemlist:
            try:
                extracted_url = self.extract_url(item['url'])
                item['originalUrl'] = extracted_url
                if extracted_url and urlresolver.isValid(extracted_url):
                    item['parsedUrl'], _ = urlresolver.resolve(extracted_url)
                    parsed.append(item)
            except Exception as e:
                    logger.exception("cineblog01")
                    continue

        # close session
        scrapertools.close_session(self.host, self.session)

        return parsed

    def list_tvshow(self, text):
        search_url = "%s/?s=%s" % ("https://www.cb01.uno/serietv", text.replace(" ", "+"))
        data, _ = scrapertools.anti_cloudflare_force(search_url, self.session)
        itemlist = []
        soup = BeautifulSoup(data, 'html.parser')
        for film_box in soup.findAll('div', {'class': 'filmbox'}):
            description_box = film_box.find('div', {'class': 'span8'})
            linked_title = description_box.find('a', href=True)
            url = linked_title['href']
            title = linked_title.text
            itemlist.append({'title': title, 'url': url})

        return itemlist

    def get_resolution(self, header_text):
        if '3D' in header_text:
            return '3D'
        
        if 'HD' in header_text:
            return 'HD'
        
        return 'SD'


    def find_movie(self, show_url):
        itemlist = []

        # Download page
        data, _ = scrapertools.anti_cloudflare_force(show_url, self.session)

        isSub = 'sub-ita' in show_url

        inserted_link = []

        soup = BeautifulSoup(data, 'html.parser')
        for link in soup.findAll('a', href=True, text='Openload'):
            if link['href'] not in inserted_link:
                previous_header = link.find_previous('strong')
                res = self.get_resolution(previous_header.text)
                inserted_link.append(link['href'])
                info = {'channel': 'cineblog01', 'sub': isSub, 'server': 'openload', 'url': link['href'], 'quality': res}
                itemlist.append(info)

        soup = BeautifulSoup(data, 'html.parser')
        for link in soup.findAll('a', href=True, text='Wstream'):
            if link['href'] not in inserted_link: 
                previous_header = link.find_previous('strong')
                res = self.get_resolution(previous_header.text)
                inserted_link.append(link['href'])
                info = {'channel': 'cineblog01', 'sub': isSub, 'server': 'wstream', 'url': link['href'], 'quality': res}
                itemlist.append(info)

        soup = BeautifulSoup(data, 'html.parser')
        for link in soup.findAll('a', href=True, text='Streamango'):
            if link['href'] not in inserted_link:
                previous_header = link.find_previous('strong')
                res = self.get_resolution(previous_header.text)
                inserted_link.append(link['href'])
                info = {'channel': 'cineblog01', 'sub': isSub, 'server': 'streamango', 'url': link['href'], 'quality': res}
                itemlist.append(info)

        soup = BeautifulSoup(data, 'html.parser')
        for link in soup.findAll('a', href=True, text='Nowvideo'):
            if link['href'] not in inserted_link:
                previous_header = link.find_previous('strong')
                res = self.get_resolution(previous_header.text)
                inserted_link.append(link['href'])
                info = {'channel': 'cineblog01', 'sub': isSub, 'server': 'nowvideo', 'url': link['href'], 'quality': res}
                itemlist.append(info)

        parsed = []
        for item in itemlist:
            try:
                extracted_url = self.new_url_extractor(item['url'])
                
                if not extracted_url:
                    print("il tuo metodo non funziona")
                    extracted_url = self.extract_url(item['url'])

                item['originalUrl'] = extracted_url
                if extracted_url and urlresolver.isValid(extracted_url):
                    item['parsedUrl'], _ = urlresolver.resolve(extracted_url)
                    parsed.append(item)
            except Exception as e:
                    logger.exception("cineblog01")
                    continue

        # close session
        scrapertools.close_session(self.host, self.session)
        
        return parsed

    def new_url_extractor(self, url):
        data, _ = scrapertools.anti_cloudflare_force(url, self.session)
        soup = BeautifulSoup(data, 'html.parser')
        node = soup.find("a", {'class': 'link'})

        if node:
            return node['href']
        
        return None

    def extract_url(self, url):
        if '/goto/' in url:
            url = url.split('/goto/')[-1].decode('base64')

        url = url.replace('http://cineblog01.uno', 'http://k4pp4.pw')

        if "go.php" in url:
            data, _ = scrapertools.anti_cloudflare_force(url, self.session)
            try:
                data = scrapertools.get_match(data, 'window.location.href = "([^"]+)";')
            except IndexError: 
                try:
                    # data = scrapertools.get_match(data, r'<a href="([^"]+)">clicca qui</a>')
                    # In alternativa, dato che a volte compare "Clicca qui per proseguire":
                    data = scrapertools.get_match(data, r'<a href="([^"]+)".*?class="btn-wrapper">.*?licca.*?</a>')
                except IndexError:
                    data = scrapertools.get_header_from_response(url, header_to_get="Location")
        #        while 'vcrypt' in data:
        #          a = scrapertools.get_header_from_response(data, self.headers=self.headers, header_to_get="Location")
        elif "/link/" in url:
            data, _  = scrapertools.anti_cloudflare_force(url, self.session)
            try:
                data = scrapertools.get_match(str(data), "(eval\(function\(p,a,c,k,e,d.*?)</script>")
                data = jsunpack.unpack(data)
            except IndexError:
                print("can't unpack " + url)

            data = scrapertools.find_single_match(data, 'var link(?:\s)?=(?:\s)?"([^"]+)";')

        #       Note. vcrypt implements "I am not a robot captcha. Can't pass automatically".
        #       The implementations below doesn't work with new version of vcrypt
        #       The code is here to handle legacy url
        #        while 'vcrypt' in d#            data = scrapertools.get_header_from_response(data, self.headers=self.headers, header_to_get="Location")
        else:
            data = url

        if 'vcrypt' in data: return ""

        return data
