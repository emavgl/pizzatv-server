import unittest
import time
import pprint

import sys
sys.path.append("../..")

from modules.helper import ShowType
from channels import seriehd

source = {'name': 'seriehd', 'service': seriehd.Seriehd}

def download_from_source(title):
    name = source['name']
    service_class = source['service']
    service = service_class()
    search_list = service.search(title, ShowType.MOVIE)
    results = []
    if search_list:
        print("> title found")
        results = service.find_movie(search_list[0].get('url'))
    else:
        print("> title not found")
    return name, results


def download_from_source_tv(title, episode=1, season=1):
    name = source['name']
    service_class = source['service']
    service = service_class()
    search_list = service.search(title, ShowType.TV)
    results = []
    if search_list:
        print('found')
        results = service.find_episode(search_list[0].get('url'), season, episode)
    else:
        print("not found")
    return name, results

class Seriehd(unittest.TestCase):

    def test_movie(self):
        with self.assertRaises(Exception) as context:
            name, results = download_from_source("")
        self.assertTrue(context.exception)

    def test_tv(self):
        name, results = download_from_source_tv("Dark", episode=4, season=1)
        print(name, "> Extracted", len(results))
        pprint.pprint(results)
        self.assertTrue(len(results) > 0)


if __name__ == '__main__':
    unittest.main()