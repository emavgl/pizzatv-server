import re
from core import fakelogger as logger
from core import scrapertools
from servers import openload
from modules import urlresolver
from modules.helper import ShowType
import logging
from bs4 import BeautifulSoup
import base64

logger = logging.getLogger(__name__)

class AltaDefinizioneClick:
    def __init__(self):
        self.host = "http://altadefinizione.pink"
        self.headers = {
            'User-Agent':  'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
            'Referer': self.host
        }
        self.session = scrapertools.open_session(self.host, cloudflare=True)

    def search(self, text, target):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def find_episode(self, show_url, season = 1, episode = 1):
        raise NotImplementedError()

    def list_tvshow(self, text):
        raise NotImplementedError()

    def list_movies(self, text):
        text = str(text).replace(" ", "+")
        search_url = "%s/?s=%s" % (self.host, text)
        itemlist = []

        data, _ = scrapertools.anti_cloudflare_force(search_url, self.session)

        soup = BeautifulSoup(data, 'html.parser')
        main_section = soup.find('section', {'id': 'lastUpdate'})
        rows = main_section.findAll('div', {'class': 'nomobile'})
        for row in rows:
            for film_box in row.findAll('div', {'class': 'wrapperImage'}):
                parent_node = film_box.parent
                url = parent_node.get('href')
                title = film_box.find('h5', {'class': 'titleFilm'}).text
                info = {'title': title, 'url': url}
                itemlist.append(info)
        
        return itemlist

    def url_decode(self, url_enc):
        lenght = len(url_enc)
        if lenght % 2 == 0:
            len2 = int(lenght / 2)
            first = url_enc[0:len2]
            last = url_enc[len2:lenght]
            url_enc = last + first
            reverse = url_enc[::-1]
            return base64.b64decode(reverse)

        last_car = url_enc[lenght - 1]
        url_enc[lenght - 1] = ' '
        url_enc = url_enc.strip()
        len1 = len(url_enc)
        len2 = len1 / 2
        first = url_enc[0:len2]
        last = url_enc[len2:len1]
        url_enc = last + first
        reverse = url_enc[::-1]
        reverse = reverse + last_car
        return base64.b64decode(reverse)

    def find_movie(self, show_url):
        # get inner iframe with sources and resolutions
        html_doc, _ = scrapertools.anti_cloudflare_force(show_url, self.session)
        id_film = re.search('idFilm=(\d+)', html_doc).group(1)
        menu_episodes = 'http://hdpass.net/film.php?idFilm=' + id_film + '&res=1080'

        # anticloudflare is no longer necessary
        html_doc, _ = scrapertools.downloadpage(menu_episodes, headers=self.headers)

        # if res=1080 - openload is the first selected 
        soup = BeautifulSoup(html_doc, 'html.parser')
        tmp = soup.find('input', {'id': "urlEmbed"})
        encrypted_link = {'enc': tmp.get('value'), 'server': 'openload'}

        video_url = self.url_decode(encrypted_link['enc'])
        video_url = video_url.decode('utf-8')
        server_name = encrypted_link['server']
        decrypted = []
        if urlresolver.isValid(video_url):
            parsed, server = urlresolver.resolve(video_url)
            decrypted.append({'server': encrypted_link['server'], 'originalUrl': video_url,
                                'channel': 'altadefinizioneclick', 'parsedUrl': parsed, 'quality': '-'})

        # close session
        scrapertools.close_session(self.host, self.session)

        return decrypted

