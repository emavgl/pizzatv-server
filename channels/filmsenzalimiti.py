from bs4 import BeautifulSoup
from modules import urlresolver
from modules.helper import ShowType
from core import scrapertools
import re
import logging
logger = logging.getLogger(__name__)

class Filmsenzalimiti:
    def __init__(self):
        self.host = "http://filmsenzalimiti.black/"
        self.headers = {
            'User-Agent':  'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
            'Referer': self.host
        }
        self.session = scrapertools.open_session(self.host)

    def search(self, text: str, target: str):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def find_episode(self, show_url, session, season = 1, episode = 1):
        raise NotImplementedError()

    def list_tvshow(self, text):
        raise NotImplementedError()

    def list_movies(self, text: str):
        text = str(text).replace(" ", "+")
        search_url = "%s/?s=%s" % (self.host, text)
        try:
            item_list = []
            search_page, _ = scrapertools.downloadpage(search_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(search_page, 'html.parser')
            main_box = soup.find("ul", {"class": "posts"})
            found_items = main_box.findAll("li")
            for fitem in found_items:
                url = fitem.a.get('href')
                thumbnail = fitem.a.get('data-thumbnail')
                title = (fitem.find("div", {"class": "title"})).text
                if 'serie-tv' not in url:
                    info = {'title': title, 'thumbnail': thumbnail, 'url': url}
                    item_list.append(info)
            return item_list
        except Exception as e:
            logger.exception("filmsenzalimiti")
            return []

    def find_movie(self, url):
        movie_page, _ = scrapertools.downloadpage(url, session=self.session, headers=self.headers)
        soup = BeautifulSoup(movie_page, 'html.parser')

        title = soup.find('h1').text
        sources = soup.findAll('iframe', {"class": "embed-responsive-item"})
        link_list = [ x.get('src') for x in sources ]
        linkbox_list = [ x.get('href') for x in soup.find("div", id="links").findAll("a") ]
        
        res = 'HD'
        if 'HD' not in title:
            res = 'SD'

        total = link_list + linkbox_list

        item_list = []
        for link in total:
            info = {'quality': res,'originalUrl': link}
            original_url = link
            if urlresolver.isValid(original_url):
                info['parsedUrl'], info['server'] = urlresolver.resolve(link)    
                item_list.append(info)
            else:
                print(original_url, "invalid")

        # close session
        scrapertools.close_session(self.host, self.session)

        # return items
        return item_list
