import pymysql
import logging
logger = logging.getLogger(__name__)

class DbConnector:
    def __init__(self):
        self.conn = pymysql.connect(host='db', user='pizzatv',
                                    password='pizzatv', db='pizzadb')

    def commit(self):
        self.conn.commit()

    def close(self):
        self.conn.close()

    def execute_query(self, query, args=None, fetchall=False):
        try:
            with self.conn.cursor(pymysql.cursors.DictCursor) as cursor:
                if args:
                    cursor.execute(query, args)
                else:
                    cursor.execute(query)

                if fetchall:
                    rows = cursor.fetchall()
                    return rows
                return True
        except Exception as e:
            logger.debug("Fail to query: " + str(query) + " error " +  str(e))
            return False

    def putShowOnDB(self, show_id, title, plot, poster, cover, showType):
        args = (show_id, title, plot, poster, cover, showType)
        query = "INSERT IGNORE INTO Show values (%s, %s, %s, %s, %s, %s)"
        self.execute_query(query, args)

    def putEpisodeOnDB(self, show_id, episode, season,
                       channel, server, original_url, parsed_url, sub, quality, showType):

        query = "REPLACE INTO Link values (%s, %s, %s, %s, %s, %s, NULL, %s, %s, %s, %s)"
        args = (show_id, episode, season, channel, server, original_url, 1, showType, sub, quality)

        self.execute_query(query, args)

    def getAllShows(self):
        retrieve_query = "SELECT * FROM Show"
        return self.execute_query(retrieve_query, fetchall=True)

    def getAllEpisodes(self):
        retrieve_query = "SELECT * FROM Link"
        return self.execute_query(retrieve_query, fetchall=True)

    def getEpisode(self, show_id, season, episode):
        retrieve_query = "SELECT * FROM Link WHERE show_id=%s AND season=%s AND episode=%s"
        args = (show_id, season, episode)
        return self.execute_query(retrieve_query, args, fetchall=True)

    def getShowTitle(self, show_id):
        retrieve_query = "SELECT title FROM Show WHERE id=%s"
        args = (show_id,)
        return self.execute_query(retrieve_query, args, fetchall=True)

    def updateParsedUrl(self, parsed_url, original_url):
        query = "UPDATE Link SET parsed_url = %s WHERE original_url = %s"
        args = (parsed_url, original_url)
        return self.execute_query(query, args)

    def deleteLink(self, original_url):
        query = "DELETE FROM Link WHERE original_url = %s"
        args = (original_url,)
        return self.execute_query(query, args)
    
    def markAsInvalid(self, original_url):
        query = "UPDATE Link SET valid = 0 WHERE original_url = %s"
        args = (original_url,)
        return self.execute_query(query, args)
