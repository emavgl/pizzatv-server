import re
import requests
from bs4 import BeautifulSoup
import base64
from modules import urlresolver
from modules.helper import ShowType
from core import scrapertools
import logging
logger = logging.getLogger(__name__)

class Seriehd:
    def __init__(self):
        self.host = "http://www.seriehd.me"
        self.hdpass_url = "http://hdpass.net"
        self.headers = {
            'User-Agent':  'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
            'Referer': self.host
        }
        self.session = scrapertools.open_session(self.host, cloudflare=True)

    def search(self, text, target):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def find_movie(self, show_url):
        raise NotImplementedError()

    def list_movies(self, text):
        raise NotImplementedError()

    def list_tvshow(self, text):
        try:
            text = text.replace(' ', '+')
            search_url = self.host + "/?s=" + text
            item_list = []
            
            html_doc, _ = scrapertools.downloadpage(search_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            found_items = soup.find_all("div", {'class': "imagen"})
            for item in found_items:
                title = item.h2.text
                thumbnail = item.find('img').get('src')
                url = item.find('a').get('href')
                item_list.append({'title': title, 'thumbnail': thumbnail, 'url': url})

            return item_list
        except Exception:
            logger.exception("seriehd")
            return []

    def url_decode(self, url_enc):
        lenght = len(url_enc)
        if lenght % 2 == 0:
            len2 = int(lenght / 2)
            first = url_enc[0:len2]
            last = url_enc[len2:lenght]
            url_enc = last + first
            reverse = url_enc[::-1]
            return base64.b64decode(reverse)

        last_car = url_enc[lenght - 1]
        url_enc[lenght - 1] = ' '
        url_enc = url_enc.strip()
        len1 = len(url_enc)
        len2 = len1 / 2
        first = url_enc[0:len2]
        last = url_enc[len2:len1]
        url_enc = last + first
        reverse = url_enc[::-1]
        reverse = reverse + last_car
        return base64.b64decode(reverse)

    def find_episode(self, show_url, season = 1, episode = 1):
        try:
            html_doc, _ = scrapertools.downloadpage(show_url, session=self.session, headers=self.headers)
            id_serie = re.search('idSerie=(\d+)', html_doc).group(1)

            menu_episodes = 'http://hdpass.net/serie.php?idSerie=' + id_serie
            html_doc, _ = scrapertools.downloadpage(menu_episodes, session=self.session, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            season_elements = soup.find("section", {'id': "series"})
            season_links = season_elements.find_all('a')

            if len(season_links) < season:
                # do not have the season
                return []

            # season is now an index for season_links
            season = season - 1

            episode_page = season_links[season].get('href') + "&episode=" + str(episode)
            episode_url =  self.hdpass_url + "/" + episode_page
            html_doc, _ = scrapertools.downloadpage(episode_url, session=self.session, headers=self.headers)
            id_film = re.search('idFilm=(\d+)', html_doc).group(1)

            base_request_url = 'http://hdpass.net/film.php?idFilm=' + str(id_film)
            html_doc, _ = scrapertools.downloadpage(base_request_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            from_field = (soup.find('input', {'name': 'from'})).get('value')

            sources = ['openload']
            encrypted = []

            for s in sources:
                payload = {'play_chosen': s, 'idFilm': id_film, 'res': '1080', 'from': from_field}
                url = "http://hdpass.net/film.php?idFilm="+ id_film + "&res=1080&from=" + from_field + "&play_chosen=" + s
                html_doc, _ = scrapertools.downloadpage(url, session=self.session, headers=self.headers)

                if 'Non ci sono file caricati' not in html_doc:
                    soup = BeautifulSoup(html_doc, 'html.parser')
                    tmp = soup.find('input', {'id': "urlEmbed"})
                    encrypted.append({'enc': tmp.get('value'), 'server': s})

            decrypted = []
            for enc in encrypted:
                video_url = self.url_decode(enc['enc'])
                video_url = video_url.decode('utf-8')
                server_name = enc['server']
                logger.info(video_url)
                if not urlresolver.isValid(video_url): continue
                parsed, server = urlresolver.resolve(video_url)
                decrypted.append({'server': enc['server'], 'originalUrl': video_url,
                                    'channel': 'seriehd', 'quality': 'HD', 'parsedUrl': parsed})

            # close session
            scrapertools.close_session(self.host, self.session)

            return decrypted
        except Exception as e:
            logger.exception("seriehd")
            return []
