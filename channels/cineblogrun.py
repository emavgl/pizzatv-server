import re
from core import fakelogger as logger
from core import scrapertools
from servers import openload
from modules import urlresolver
from modules.helper import ShowType
from modules.helper import compare_strings
import logging
from bs4 import BeautifulSoup
import base64

logger = logging.getLogger(__name__)

class CineblogRun:
    def __init__(self):
        self.host = "http://www.cineblog.run"
        self.headers = {
            'User-Agent':  'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
            'Referer': self.host
        }
        self.session = scrapertools.open_session(self.host)

    def search(self, text, target):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def find_episode(self, show_url, season=1, episode=1):
        data, _ = scrapertools.downloadpage(show_url, session=self.session)
        soup = BeautifulSoup(data, 'html.parser')
        play_links = soup.findAll('td', {'class': 'MvTbPly'})
        play_links = [x.find('a') for x in play_links]

        season_episode_str = '-{0}x{1}'.format(str(season), str(episode))
        episode_link = [x['href'] for x in play_links if season_episode_str in x['href']]

        results = []
        if episode_link:
            data, _ = scrapertools.downloadpage(url=episode_link[0], session=self.session)
            soup = BeautifulSoup(data, 'html.parser')
            stream_buttons = soup.findAll('a', {'class': 'STPb'})

            for stream_button in stream_buttons:
                video_url = stream_button.get('href')
                video_url = urlresolver.followRedirectUrl(video_url)

                if not video_url: continue
                if not 'http' in video_url:
                    video_url = 'http:' + video_url

                if urlresolver.isValid(video_url):
                    parsed, server = urlresolver.resolve(video_url)
                    results.append({'server': server, 'originalUrl': video_url,
                                    'channel': 'cineblogrun', 'quality': 'SD', 'parsedUrl': parsed})

        return results

    def list_tvshow(self, text):
        search_url = self.host + '/?s=' + text.replace(' ', '+')
        data, _ = scrapertools.downloadpage(search_url, session=self.session)

        soup = BeautifulSoup(data, 'html.parser')
        result_box = soup.find('ul', {'class': 'MovieList'})
        articles = result_box.findAll('li', {'class': 'TPostMv'})
        tv_shows = [x for x in articles if x.find('span', {'class': 'TpTv'})]

        results = []

        for show in tv_shows:
            link = (show.find('a'))['href']
            title = (show.find('h3', {'class': 'Title'})).text
            if compare_strings(title, text):
                info = {'title': title, 'url': link}
                results.append(info)

        return results

    def list_movies(self, text):
        search_url = self.host + '/?s=' + text.replace(' ', '+')
        data, _ = scrapertools.downloadpage(search_url, session=self.session)

        soup = BeautifulSoup(data, 'html.parser')
        result_box = soup.find('ul', {'class': 'MovieList'})
        shows = result_box.findAll('li', {'class': 'TPostMv'})

        results = []

        for show in shows:
            link = (show.find('a'))['href']
            title = (show.find('h3', {'class': 'Title'})).text
            if compare_strings(title, text):
                info = {'title': title, 'url': link}
                results.append(info)

        return results

    def get_resolution(self, text):
        if '1080' in text or '720' in text:
            return 'HD'
        else:
            return 'SD'

    def find_movie(self, show_url):
        data, _ = scrapertools.downloadpage(show_url, session=self.session)
        soup = BeautifulSoup(data, 'html.parser')

        # Get tables with all the links
        table = soup.find('tbody')
        rows = table.findAll('tr')

        results = []
        for row in rows:
            
            # resolution
            spans = row.findAll('span')
            res = self.get_resolution(spans[-1].text)
            
            # extract link
            link = row.find('a', {'class': 'Button STPb'})

            video_url = link.get('href')
            video_url = urlresolver.followRedirectUrl(video_url)

            if not video_url: continue
            if not 'http' in video_url:
                video_url = 'http:' + video_url
            
            if urlresolver.isValid(video_url):
                parsed, server = urlresolver.resolve(video_url)
                results.append({'server': server, 'originalUrl': video_url,
                                'channel': 'cineblogrun', 'quality': res, 'parsedUrl': parsed})

        # close session
        scrapertools.close_session(self.host, self.session)

        return results