import re
import requests
from core import scrapertools
from core import jsunpack

def isValid(url):
    try:
        r = requests.get(url, timeout=6)
        html_doc = r.text
        if r.status_code != 200 or 'no longer exists' in html_doc:
            return False
        return True
    except Exception as e:
        return False

def resolve(url, data):
    data_pack = scrapertools.find_single_match(data, "(eval.function.p,a,c,k,e,.*?)\s*</script>")
    video_url = ""
    if data_pack != "":
        data = jsunpack.unpack(data_pack)
        video_url = scrapertools.find_single_match(data, 'file"?\s*:\s*"([^"]+)",')
    return video_url