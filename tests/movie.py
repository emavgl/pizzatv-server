import unittest

import sys
sys.path.append("..")

from modules.helper import ShowType
from channels import filmsenzalimiti
from channels import altadefinizioneclick
from channels import cineblog01
from channels import casacinema
from channels import italiafilm
from channels import cineblogrun
from channels import geniodellostreaming

sources = [
    {'name': 'casacinema', 'service': casacinema.Casacinema},
    {'name': 'altadefinizione', 'service': altadefinizioneclick.AltaDefinizioneClick},
    {'name': 'cb01', 'service': cineblog01.Cineblog},
    {'name': 'italiafilm', 'service': italiafilm.Italiafilm },
    {'name': 'filmsenzalimiti', 'service': filmsenzalimiti.Filmsenzalimiti },
    {'name': 'geniodellostreaming', 'service': geniodellostreaming.Geniostreaming },
    {'name': 'cineblogrun', 'service': cineblogrun.CineblogRun }
]

def download_from_source(source):
    name = source['name']
    service_class = source['service']
    service = service_class()
    search_list = service.search("wonder woman", ShowType.MOVIE)
    results = []
    if search_list:
        print("> title found")
        results = service.find_movie(search_list[0].get('url'))
    else:
        print("> title not found")
    return name, results

def sequential():
    merge_results = []
    for name, results in map(download_from_source, sources):
        print('from %s got %d' % (name, len(results)))
        merge_results += results
    print('merge_results %d' % (len(merge_results),))


class TestMovieSources(unittest.TestCase):

    def test_cineblog(self):
        source = {'name': 'cb01', 'service': cineblog01.Cineblog}
        name, results = download_from_source(source)
        print(name, "> Extracted", len(results))
        self.assertTrue(len(results) > 0)

    def test_casacinema(self):
        source = {'name': 'casacinema', 'service': casacinema.Casacinema}
        name, results = download_from_source(source)
        print(name, "> Extracted", len(results))
        self.assertTrue(len(results) > 0)

    def test_italiafilm(self):
        source = {'name': 'italiafilm', 'service': italiafilm.Italiafilm }
        name, results = download_from_source(source)
        print(name, "> Extracted", len(results))
        self.assertTrue(len(results) > 0)

    def test_altadefinizioneclick(self):
        source = {'name': 'altadefinizione', 'service': altadefinizioneclick.AltaDefinizioneClick}
        name, results = download_from_source(source)
        print(name, "> Extracted", len(results))
        self.assertTrue(len(results) > 0)

    def test_filmsenzalimiti(self):
        source = {'name': 'filmsenzalimiti', 'service': filmsenzalimiti.Filmsenzalimiti }
        name, results = download_from_source(source)
        print(name, "> Extracted", len(results))
        self.assertTrue(len(results) > 0)

    def test_cineblogrun(self):
        source = {'name': 'cineblogrun', 'service': cineblogrun.CineblogRun }
        name, results = download_from_source(source)
        print(name, "> Extracted", len(results))
        self.assertTrue(len(results) > 0)

    def test_geniodellostreaming(self):
        source = {'name': 'geniodellostreaming', 'service': geniodellostreaming.Geniostreaming }
        name, results = download_from_source(source)
        print(name, "> Extracted", len(results))
        self.assertTrue(len(results) > 0)

if __name__ == '__main__':
    unittest.main()