import os
import time
from urllib.parse import quote, urlparse
from http.cookiejar import MozillaCookieJar
import urllib
from threading import Lock

from core.cloudflare import Cloudflare
import core.fakelogger as logger

cookies_lock = Lock()

cj = MozillaCookieJar()

COOKIES_PATH = './cookies_2'

if not os.path.exists(COOKIES_PATH):
  os.mkdir(COOKIES_PATH)

if not os.path.exists(COOKIES_PATH):
  os.mkdir(COOKIES_PATH)

ficherocookies = os.path.join(COOKIES_PATH, "cookies.dat")

# Headers por defecto, si no se especifica nada
default_headers = dict()
default_headers["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0"
default_headers["Accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
default_headers["Accept-Language"] = "it-IT,it;q=0.8,en-US;q=0.5,en;q=0.3"
default_headers["Accept-Charset"] = "UTF-8"
default_headers["Accept-Encoding"] = "gzip"


def get_url_headers(url):
    domain_cookies = cj._cookies.get("." + urlparse(url)[1], {}).get("/", {})

    if "|" in url or not "cf_clearance" in domain_cookies:
        return url

    headers = dict()
    headers["User-Agent"] = default_headers["User-Agent"]
    headers["Cookie"] = "; ".join(["%s=%s" % (c.name, c.value) for c in domain_cookies.values()])

    return url + "|" + "&".join(["%s=%s" % (h, headers[h]) for h in headers])


def load_cookies():
    cookies_lock.acquire()
    if os.path.isfile(ficherocookies):
        logger.info("Leyendo fichero cookies")
        try:
            cj.load(ficherocookies, ignore_discard=True)
        except:
            logger.info("El fichero de cookies existe pero es ilegible, se borra")
            os.remove(ficherocookies)
    cookies_lock.release()


def save_cookies():
    cookies_lock.acquire()
    logger.info("Guardando cookies...")
    cj.save(ficherocookies, ignore_discard=True)
    cookies_lock.release()


load_cookies()


def downloadpage(url, post=None, headers=None, timeout=None, follow_redirects=True, cookies=True, replace_headers=False,
                 add_referer=False, only_headers=False, bypass_cloudflare=True):

    response = {}

    # Headers por defecto, si no se especifica nada
    request_headers = default_headers.copy()

    # Headers pasados como parametros
    if headers is not None:
        if not replace_headers:
            request_headers.update(dict(headers))
        else:
            request_headers = dict(headers)

    if add_referer:
        request_headers["Referer"] = "/".join(url.split("/")[:3])

    url = quote(url, safe="%/:=&?~#+!$,;'@()*[]")

    # Handlers
    handlers = [urllib.request.HTTPHandler(debuglevel=False)]

    if not follow_redirects:
        handlers.append(NoRedirectHandler())

    if cookies:
        handlers.append(urllib.request.HTTPCookieProcessor(cj))

    opener = urllib.request.build_opener(*handlers)

    # Contador
    inicio = time.time()

    req = urllib.request.Request(url, post, request_headers)

    try:
        if urllib.request.__version__ == "2.4":
            import socket
            deftimeout = socket.getdefaulttimeout()
            if timeout is not None:
                socket.setdefaulttimeout(timeout)
            handle = opener.open(req)
            socket.setdefaulttimeout(deftimeout)
        else:
            handle = opener.open(req, timeout=timeout)

    except urllib.request.HTTPError as handle:
        response["sucess"] = False
        response["code"] = handle.code
        response["error"] = handle.__dict__.get("reason", str(handle))
        response["headers"] = handle.headers.dict
        if not only_headers:
            response["data"] = handle.read()
        else:
            response["data"] = ""
        response["time"] = time.time() - inicio
        response["url"] = handle.geturl()

    except Exception as e:
        response["sucess"] = False
        response["code"] = e.__dict__.get("errno", e.__dict__.get("code", str(e)))
        response["error"] = e.__dict__.get("reason", str(e))
        response["headers"] = {}
        response["data"] = ""
        response["time"] = time.time() - inicio
        response["url"] = url

    else:
        response["sucess"] = True
        response["code"] = handle.code
        response["error"] = None
        response["headers"] = handle.headers.dict
        if not only_headers:
            response["data"] = handle.read()
        else:
            response["data"] = ""
        response["time"] = time.time() - inicio
        response["url"] = handle.geturl()

    logger.info("Terminado en %.2f segundos" % (response["time"]))
    logger.info("Response sucess: %s" % (response["sucess"]))
    logger.info("Response code: %s" % (response["code"]))
    logger.info("Response error: %s" % (response["error"]))
    logger.info("Response data length: %s" % (len(response["data"])))
    logger.info("Response headers:")
    for header in response["headers"]:
        logger.info("- %s: %s" % (header, response["headers"][header]))

    if cookies:
        save_cookies()

    logger.info("Encoding: %s" % (response["headers"].get('content-encoding')))

    # Anti Cloudflare
    if bypass_cloudflare:
        cf = Cloudflare(response)
        if cf.is_cloudflare:
            logger.info("cloudflare detectado, esperando %s segundos..." % cf.wait_time)
            auth_url = cf.get_url()
            logger.info("Autorizando... url: %s" % auth_url)
            if downloadpage(auth_url, headers=request_headers, replace_headers=True).sucess:
                logger.info("Autorización correcta, descargando página")
                resp = downloadpage(url=response["url"], post=post, headers=headers, timeout=timeout,
                                    follow_redirects=follow_redirects,
                                    cookies=cookies, replace_headers=replace_headers, add_referer=add_referer)
                response["sucess"] = resp.sucess
                response["code"] = resp.code
                response["error"] = resp.error
                response["headers"] = resp.headers
                response["data"] = resp.data
                response["time"] = resp.time
                response["url"] = resp.url
            else:
                logger.info("No se ha podido autorizar")

    return type('HTTPResponse', (), response)


class NoRedirectHandler(urllib.request.HTTPRedirectHandler):
    def http_error_302(self, req, fp, code, msg, headers):
        infourl = urllib.response.addinfourl(fp, headers, req.get_full_url())
        infourl.status = code
        infourl.code = code
        return infourl

    http_error_300 = http_error_302
    http_error_301 = http_error_302
    http_error_303 = http_error_302
    http_error_307 = http_error_302