import unittest
import time
import pprint

import sys
sys.path.append("../..")

from modules.helper import ShowType
from channels import geniodellostreaming

source = {'name': 'geniodellostreaming', 'service': geniodellostreaming.Geniostreaming}

def download_from_source(title):
    name = source['name']
    service_class = source['service']
    service = service_class()
    search_list = service.search(title, ShowType.MOVIE)
    results = []
    if search_list:
        print("> title found")
        results = service.find_movie(search_list[0].get('url'))
    else:
        print("> title not found")
    return name, results


def download_from_source_tv(title, episode=1, season=1):
    name = source['name']
    service_class = source['service']
    service = service_class()
    search_list = service.search(title, ShowType.TV)
    results = []
    if search_list:
        print('found')
        results = service.find_episode(search_list[0].get('url'), season, episode)
    else:
        print("not found")
    return name, results


class Geniostreaming(unittest.TestCase):

    def test_movie(self):
        name, results = download_from_source("blade runner 2049")
        print(name, "> Extracted", len(results))
        pprint.pprint(results)
        self.assertTrue(len(results) > 0)

    def test_tv(self):
        name, results = download_from_source_tv("12 monkeys", episode=3, season=3)
        print(name, "> Extracted", len(results))
        pprint.pprint(results)
        self.assertTrue(len(results) > 0)


if __name__ == '__main__':
    unittest.main()