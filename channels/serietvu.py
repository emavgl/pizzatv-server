import re
from bs4 import BeautifulSoup
from servers import speedvideo
from modules import urlresolver
from modules import helper
from modules.helper import ShowType
from core import scrapertools
import logging
logger = logging.getLogger(__name__)

class Serietvu:
    def __init__(self):
        self.host = "https://www.serietvu.online"
        self.headers = {
            'User-Agent':  'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
            'Referer': self.host
        }
        self.session = scrapertools.open_session(self.host)

    def search(self, text, target):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def find_movie(self, show_url):
        raise NotImplementedError()

    def list_movies(self, text):
        raise NotImplementedError()

    def list_tvshow(self, text):
        try:
            text = text.replace(' ', '+')
            search_url =  self.host + "/?s=" + text
            
            item_list = []
            
            html_doc, _ = scrapertools.downloadpage(search_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            found_items = soup.find_all("div", {'class': "item"})
            for item in found_items:
                tmp = item.find("a")
                url = tmp.get('href')
                thumbnail = tmp.get('data-original')
                tmp = item.find("div", {'class': "title"})
                title = tmp.text.replace("\n", "").strip()
                matching = helper.compare_strings(text, title)
                item_list.append({'title': title, 'thumbnail': thumbnail, 'url': url})

            return item_list
        except Exception:
            logger.exception("serietvu")
            return []

    def find_episode(self, show_url, season = 1, episode = 1):
        try:
            html_doc, _ = scrapertools.downloadpage(show_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            season_elements = soup.find_all("div", {'class': "list"})

            item_list = []

            list_episodes = []
            for season_element in season_elements:
                season_list = []
                for episode_element in season_element.find_all("a", {'class': "inner"}):
                    season_list.append({'url': episode_element.get('data-href'), 'title': episode_element.get('data-id')})
                list_episodes.append(season_list)

            if len(list_episodes) >= season and len(list_episodes[season]) >= episode:
                # season and episode are now indexes of the list episodes
                season = season - 1
                episode = episode - 1

                requested_video_url = (list_episodes[season][episode]).get('url')
                requested_title = (list_episodes[season][episode]).get('title').lower()
                isSub = 'sub-' in requested_title or 'sub ' in requested_title

                if urlresolver.isValid(requested_video_url):
                    extracted_url, service = urlresolver.resolve(requested_video_url)
                    item_list.append({'channel': 'serietvu', 'quality': 'SD', 'originalUrl': requested_video_url,
                                        'server': service, 'parsedUrl': extracted_url, 'sub': isSub})

            # close session
            scrapertools.close_session(self.host, self.session)

            return item_list
        except Exception as e:
            logger.exception("serietvu")
            return []
