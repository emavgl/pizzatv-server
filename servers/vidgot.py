import re
import time
from core import scrapertools
from core import jsunpack
import requests

def isValid(url):
    try:
        r = requests.get(url, timeout=6)
        html_doc = r.text
        if r.status_code != 200 or 'Not Found' in html_doc:
            return False
        return True
    except:
        return False

# Returns an array of possible video url's from the page_url
def resolve(page_url, data):
    data_pack = scrapertools.find_single_match(data, "(eval.function.p,a,c,k,e,.*?)\s*</script>")
    video_url = ""
    if data_pack != "":
        data = jsunpack.unpack(data_pack)
        video_url = scrapertools.find_single_match(data, 'file"?\s*:\s*"([^"]+mp4)"')
    
    return video_url