import requests
import re
import json
from modules.helper import ShowType
from modules import helper
import logging
logger = logging.getLogger(__name__)

API_KEY = "62d33f3b98ca88f6c5ce8472ebb92b35"
SEARCH_MOVIE_URL = "https://api.themoviedb.org/3/search/movie"
SEARCH_TV_URL = "https://api.themoviedb.org/3/search/tv"
POSTER_BASE = "https://image.tmdb.org/t/p/w500"
POSTER_BASE_THUMBNAIL = "https://image.tmdb.org/t/p/w185"
BACKDROP_BASE = "https://image.tmdb.org/t/p/original"
BACKDROP_BASE_THUMBNAIL = "https://image.tmdb.org/t/p/w300"
TV_BASE = "https://api.themoviedb.org/3/tv/"
MOVIE_BASE = "https://api.themoviedb.org/3/movie/"

def create_show(show):
    if show['poster_path']:
        show['poster_path_thumbnail'] = POSTER_BASE_THUMBNAIL + show['poster_path']
        show['poster_path'] = POSTER_BASE + show['poster_path']
    if show['backdrop_path']:
        show['backdrop_path_thumbnail'] = BACKDROP_BASE_THUMBNAIL + show['backdrop_path']
        show['backdrop_path'] = BACKDROP_BASE + show['backdrop_path']

    if 'title' not in show:
        show['title'] = show['name']

    return show

def search(title, target):
    try:
        BASE_URL = SEARCH_MOVIE_URL
        if target == ShowType.TV:
            BASE_URL = SEARCH_TV_URL

        # clean title
        title = helper.clean(title)

        # create payload
        payload = {
                    'api_key': API_KEY, 'language': 'it',
                    'query': title, 'page': 1
                  }

        # send request
        res = requests.get(BASE_URL, payload)
        dic_res = json.loads(res.text)

        # retrieve shows
        results = []
        if dic_res['total_results'] > 0:
            for res in dic_res['results']:
                results.append(create_show(res))
        return results
    except Exception as e:
        logger.exception('tmdb info')
        return []
    
def getPopularShows(target):
    payload = {'api_key': API_KEY, 'language': 'it'}
    url_path = MOVIE_BASE
    if target == ShowType.TV:
        url_path = TV_BASE
    url_path = url_path + "popular"
    res = requests.get(url_path, payload)
    popular = json.loads(res.text)
    popular = popular['results']
    item_list = []
    for pop in popular:
        item_list.append(create_show(pop))
    return item_list

def getSeasonInfo(showId, season):
    url_path = TV_BASE
    url_path += showId + "/season/" + str(season)
    payload = {'api_key': API_KEY, 'language': 'it'}
    res = requests.get(url_path, payload)
    seasonInfo = json.loads(res.text)
    episodeList = seasonInfo['episodes']
    if seasonInfo['poster_path']:
        seasonInfo['poster_path'] = POSTER_BASE + seasonInfo['poster_path']
    for episode in episodeList:
        if episode['still_path']:
            episode['still_path'] = BACKDROP_BASE + episode['still_path']
            episode['still_path_thumbnail'] = BACKDROP_BASE_THUMBNAIL + episode['still_path']
            
            # uniformity reason
            episode['backdrop_path'] = episode['still_path']
            episode['backdrop_path_thumbnail'] = episode['still_path_thumbnail'] 

    return seasonInfo

def getSuggestedShows(target, showId):
    url_path = MOVIE_BASE
    if target == ShowType.TV:
        url_path = TV_BASE
    payload = {'api_key': API_KEY, 'language': 'it'}
    url_path = url_path + showId + "/recommendations"
    res = requests.get(url_path, payload)
    suggested = json.loads(res.text)
    suggested = suggested['results']
    item_list = []
    for pop in suggested:
        item_list.append(create_show(pop))
    return item_list

def getInfo(title, showid, target):
    logger.info('tmdb info')
    try:
        if not showid:
            results = search(title, target)
            showid = (results[0])['id']

        # search by id
        if target == ShowType.TV:
            url = TV_BASE + str(showid)
            payload = {'api_key': API_KEY, 'language': 'it'}
        else:
            # target is movie
            url = MOVIE_BASE + str(showid)
            payload = {'api_key': API_KEY, 'language': 'it'}

        res = requests.get(url, payload)
        showInfo = json.loads(res.text)
        showInfo = create_show(showInfo)

        # Download videos
        if target == ShowType.MOVIE:
            url = MOVIE_BASE + str(showid) + "/videos"
            payload = {'api_key': API_KEY, 'language': 'it'}
            res = requests.get(url, payload)
            videoInfo = json.loads(res.text)
            showInfo['videos'] = []
            for video in videoInfo['results']:
                if video['site'] == 'YouTube':
                    embed_url = 'https://www.youtube.com/embed/' + video['key']
                    showInfo['videos'].append({'url': embed_url, 'id': video['key'], 'title': video['name']})
        
        return showInfo
    except Exception as e:
        logger.exception('tmdb info')
        return None