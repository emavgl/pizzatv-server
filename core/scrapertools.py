﻿# -*- coding: utf-8 -*-
import os
import re
from urllib.parse import urlparse, urlsplit, quote_plus
import time
import cfscrape
import requests, requests.utils, pickle
import logging
from core.cloudflare import Cloudflare
from core import httptools
logger = logging.getLogger(__name__)

COOKIES_PATH = './cookies'

DEFAULT_HEADERS = {
    'User-Agent':  'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0'
}

if not os.path.exists(COOKIES_PATH):
  os.mkdir(COOKIES_PATH)

# 10 seconds
DEFAULT_TIMEOUT = 10

def downloadpage(url, session=None, request_type='get', data=None, cookies=None, headers=DEFAULT_HEADERS):
    """
    Wrapper for HTTP requests using Request library
    """
    s = session
    if s is None:
        s = open_session(url, cookies=cookies)

    # requests
    if request_type == 'get':
        response = s.get(url, data=data, headers=headers, timeout=DEFAULT_TIMEOUT)
    else:
        response = s.post(url, data=data, headers=headers, timeout=DEFAULT_TIMEOUT)

    if session is None:
        # means that I am not interested in mantain the session active
        close_session(url, s)

    return response.text, response

def open_session(host, cookies=None, cloudflare=False):
    """
    returns a new requests's session and set cookies on it
    if cookies are not specified, it looks for saved cookies
    """
    if cloudflare:
        _, session = anti_cloudflare_force(host, None)
        return session

    # get cookie filename
    dominio = urlparse(host)[1].replace("www.", "")
    path_cookies = os.path.join(COOKIES_PATH, dominio + ".dat" )

    # get cookies
    cj = None
    if cookies is not None:
        cj = requests.utils.cookiejar_from_dict(cookies)
    else:
        # if cookies exists, import it
        if os.path.isfile(path_cookies):
            # if we have a cookie file already saved
            # then load the cookies into the Cookie Jar
            try:
                with open(path_cookies, 'rb') as f:
                    cj = requests.utils.cookiejar_from_dict(pickle.load(f))
            except Exception as e:
                # Error while reading cookies
                os.remove(path_cookies)

    # open new session
    s = requests.Session()
    if cj is not None:
        s.cookies = cj

    return s

def close_session(host, session):
    """
    Close the requests' session and save cookies on file
    """
    # get dominio
    dominio = urlparse(host)[1].replace("www.", "")
    path_cookies = os.path.join(COOKIES_PATH, dominio + ".dat" )

    # save cookie
    with open(path_cookies, 'wb') as f:
        pickle.dump(requests.utils.dict_from_cookiejar(session.cookies), f)

    # close session
    session.close()

def get_header_from_response(url, header_to_get):
    content, response = downloadpage(url)
    return response.headers.get(header_to_get)

# Useful method for regex
def get_match(data,patron,index=0):
    matches = re.findall( patron , data , flags=re.DOTALL )
    return matches[index]

def find_single_match(data,patron,index=0):
    try:
        matches = re.findall( patron , data , flags=re.DOTALL )
        return matches[index]
    except:
        return ""

def find_multiple_matches(text,pattern):
    return re.findall(pattern,text,re.DOTALL)

# Videomega and filmsenzalimiti dependency
def get_filename_from_url(url):
    parsed_url = urlparse(url)
    try:
        filename = parsed_url.path
    except:
        # Si falla es porque la implementación de parsed_url no reconoce los atributos como "path"
        if len(parsed_url)>=4:
            filename = parsed_url[2]
        else:
            filename = ""

    if "/" in filename:
        filename = filename.split("/")[-1]

    return filename

# anti_cloudflare dependency
def parseJSString(s):
    try:
        offset = 1 if s[0] == '+' else 0
        val = int(eval(s.replace('!+[]', '1').replace('!![]', '1').replace('[]', '0').replace('(', 'str(')[offset:]))
        return val
    except:
        pass

# Method 1: cloudflare
# TODO: dismiss
def anti_cloudflare(url):
    content, response = downloadpage(url)
    try:
        cloud_dict = {'url': url, 'data': content, 'headers': response.headers}
        cf = Cloudflare(cloud_dict)
        if cf.is_cloudflare:
            auth_url = cf.get_url()
            r, _ = downloadpage(auth_url)
            header = DEFAULT_HEADERS
            header['Referer'] = "/".join(url.split("/")[:3])
            content, _ = downloadpage(url, header)
            # TODO: use http.download page instead
        return content
    except:
        logger.exception("cloudflare_fails")
        return content

def anti_cloudflare_force(url, ses):
    try:
        scraper = None
        if ses is not None:
            scraper = ses
        else:
            scraper = cfscrape.create_scraper()
        data = scraper.get(url, timeout=DEFAULT_TIMEOUT).content
        data = data.decode("utf-8", errors="ignore")
        return data, scraper
    except Exception as e:
        print("forcecloudflare-error", e)
        scraper.close()
        logger.exception("forcecloudflare")
        return None, None