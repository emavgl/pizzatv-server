import re
from bs4 import BeautifulSoup
from modules import urlresolver
from core import scrapertools
import logging
from modules.helper import ShowType
logger = logging.getLogger(__name__)

class Vediserie:
    def __init__(self):
        self.host = "http://www.vediserie.com/"
        self.headers = {
            'User-Agent':  'Mozilla/5.0 (Windows NT 6.1; rv:38.0) Gecko/20100101 Firefox/38.0',
            'Referer': self.host
        }
        self.session = scrapertools.open_session(self.host)
        
    def search(self, text, target):
        if target == ShowType.MOVIE:
            return self.list_movies(text)
        else:
            return self.list_tvshow(text)

    def find_movie(self, show_url):
        raise NotImplementedError()

    def list_movies(self, text):
        raise NotImplementedError()

    def list_tvshow(self, text):
        try:
            text = text.replace(' ', '+')
            search_url =  self.host + "/?s=" + text

            item_list = []

            html_doc, _ = scrapertools.downloadpage(search_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            found_items = soup.find_all("div", {'class': "imagen"})
            for item in found_items:
                thumbnail = item.find("img").get('src')
                title = item.h2.text
                tmp = item.find("a")
                url = tmp.get('href')
                item_list.append({'title': title, 'thumbnail': thumbnail, 'url': url})
            return item_list
        except Exception:
            logger.exception("vediserie")
            return []

    def find_episode(self, show_url, season = 1, episode = 1):
        try:
            item_list = []
            available_seasons = []

            html_doc, _ = scrapertools.downloadpage(show_url, session=self.session, headers=self.headers)
            soup = BeautifulSoup(html_doc, 'html.parser')
            season_elements = soup.find_all("div", {'class': "listEp"})

            season_list_box = soup.find("ul", {'class': "listSt"})
            available_link_seasons = season_list_box.find_all('a')
            available_seasons = [int(s.text) for s in available_link_seasons]

            # check if there is matching season, if not return
            if season not in available_seasons: return []

            # get the matching season_element
            season_element = season_elements[available_seasons.index(season)]

            # getting all the episode inside the matching season_element
            requested_video_url = None
            for episode_element in season_element.find_all('a'):
                # parsing the number in strings like "12" or "12 Sub-Ita"
                episode_string = re.findall(r'\d+', episode_element.text)
                if not episode_string: continue # skip the episode
                if int(episode_string[0]) == episode:
                    requested_video_url = episode_element.get('data-link')
                    requested_title = episode_element.text.lower()
                    isSub = 'sub-' in requested_title or 'sub ' in requested_title
                    break

            logging.debug("requested file url")
            logging.debug(requested_video_url)
            if not requested_video_url: return []
            if urlresolver.isValid(requested_video_url):
                logging.debug("is valid")
                extracted_url, service = urlresolver.resolve(requested_video_url)
                item_list.append({'channel': 'vediserie', 'quality': 'SD', 'sub': isSub, 'episode': episode, 'season': season, 'originalUrl': requested_video_url,
                                    'server': service, 'parsedUrl': extracted_url})

            # close session
            scrapertools.close_session(self.host, self.session)

            return item_list
        except Exception as e:
            logger.exception("vediserie")
            return []
