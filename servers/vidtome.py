import re
import base64
from core import fakelogger as logger
from core import scrapertools
import requests
import logging

logger = logging.getLogger(__name__)

def isValid(url):
    try:
        r = requests.get(url, timeout=6)
        html_doc = r.text
        if 'Expired' in html_doc or 'File was deleted' in html_doc:
            return False
        return True
    except Exception as e:
        logger.exception("vidtome valid")
        return False

def resolve(page_url, data):
    try:
        # op = scrapertools.find_single_match(data, 'name="op" value="([^"]+)"')
        # id_p = scrapertools.find_single_match(data, 'name="id" value="([^"]+)"')
        # fname = scrapertools.find_single_match(data, 'name="fname" value="([^"]+)"')
        # hash_p = scrapertools.find_single_match(data, 'name="hash" value="([^"]+)"')
        # imhuman = scrapertools.find_single_match(data, 'name="imhuman" value="([^"]+)"')

        # payload = {'op': op, 'referer': '', 'usr_login': '', 'id': id_p,
        #             'fname': fname, 'hash': hash_p, 'imhuman': 'Proceed+to+video'
        # }

        # data, _ = scrapertools.downloadpage(page_url, session=None, request_type='post', data=payload)

        media_urls = scrapertools.find_multiple_matches(data, '\{file\s*:\s*"([^"]+)",label\s*:\s*"([^"]+)"\}')
        url, _ = media_urls[0]
        return url
    except Exception as e:
        logger.exception("vidtome_resolve" + page_url)
        return None