requests
Flask
BeautifulSoup4
cfscrape>=1.9.5
gunicorn
pymysql
celery
flask-cors